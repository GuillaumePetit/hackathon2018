<?php

namespace App\Http\Controllers;

use App\Calendar;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class APIController extends Controller
{
    public function initBDD()
    {
        if (($handle = fopen("ADE-extract.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $data = array_map('utf8_encode', $data);
                //dd($data);

                $c = Calendar::create([
                    'formation' => $data[0],
                    'date' => Carbon::createFromFormat('d/m/y', $data[1]),
                    'duree' => $data[2],
                    'heure_debut' => $data[3],
                    'intitule' => $data[4],
                    'lieu' => $data[5],
                    'enseignant' => $data[6]
                ]);
                //Calendar::create($data[1]);
            }
            fclose($handle);
        }
    }

    //Salle en fonction d'une formation et d'une date
    public function getCours()
    {

        $date_unformate = Input::get('date');
        $params = [];
        if (empty($date_unformate)) {
            $params['date'] = Carbon::now()->format('Y-m-d');
        } else {
            $params['date'] = Carbon::createFromFormat('Y-m-d', $date_unformate)->format('Y-m-d');
        }
        $params['formation'] = Input::get('formation');
        $params['heure_debut'] = Input::get('heure_debut');
        $params['enseignant'] = Input::get('enseignant');
        $params['lieu'] = Input::get('salle');
        $params['intitule'] = Input::get('intitule');
        $nb_semaine = Input::get('nb_semaine');
        $comparateur = Input::get('comparateur');
        if (empty($comparateur)) $comparateur = '=';

        if (!empty($nb_semaine)) {
            $res = $this->traitementRequeteSemaine($params, $nb_semaine);
        } else {
            $res = $this->traitementRequeteCours($params, $comparateur);
        }
        $res = $this->traitementErrors($res, $params);

        return json_encode($res);
    }

    private function traitementRequeteSemaine($params, $nb_semaine)
    {
        $res = '';
        $query = "SELECT calendars.formation, calendars.date, calendars.duree, calendars.heure_debut, calendars.intitule, calendars.lieu, calendars.enseignant FROM calendars WHERE ";
        $params['date'] == Carbon::now()->setISODate(Carbon::now()->year, Carbon::now()->weekOfYear + $nb_semaine)->startOfWeek();
        $empty_param = "";
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                $empty_param = $key;
            }
        }
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                if ($key == 'date') {
                    $query .= 'calendars.' . $key . '>' . '"' . $param . '"';
                }else{
                    $query .= 'calendars.' . $key  . '="' . $param . '"';
                }


                if ($empty_param != $key) {
                    $query .= " AND ";
                }
            }
        }
       // dd($params, $query);

        $res = DB::select($query);

        return $res;
    }

    public function getSynchro(){
        return json_encode(Calendar::where('formation', Input::get('formation'))->get());
    }

    public function getExamen(){
        return json_encode(Calendar::where('formation', Input::get('formation'))
            ->where('intitule', 'LIKE', '%examen%')
            ->where('date', '>=', Carbon::now())->get());

    }



    private function traitementRequeteCours($params, $comparateur)
    {
        $res = '';
        $query = "SELECT calendars.formation, calendars.date, calendars.duree, calendars.heure_debut, calendars.intitule, calendars.lieu, calendars.enseignant FROM calendars WHERE ";


        $empty_param = "";
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                $empty_param = $key;
            }
        }
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                if ($key == 'date' || $key == 'heure_debut') {
                    $query .= 'calendars.' . $key . $comparateur . '"' . $param . '"';
                } else {
                    $query .= 'calendars.' . $key . '=' . '"' . $param . '"';
                }
                if ($empty_param != $key) {
                    $query .= " AND ";
                }
            }
        }
        //dd($query);


        $res = DB::select($query);

        return $res;

    }

    private function traitementErrors($res, $params)
    {
        if (!sizeof($res)) {//Si la quarry est vide
            foreach ($params as $key => $param) {
                if (!empty($param)) {
                    $calendar = Calendar::where($key, $param)->get();
                    if (sizeof($calendar) == 0) {// Si la requête est vide
                        if ($key == 'date') {
                            $res['error']['type'] = $key;
                            $res['error']['value'] = $param;
                        } else {
                            $res['error']['type'] = $key;
                            $res['error']['value'] = $param;
                        }
                    } else {
                        //$res .= 'INFO : parameter ' . $key . ' with value : ' . $param . ' return ' . sizeof($calendar) . ' values \n';
                    }
                } else {
                    //$res .= 'WARNING : parameter ' . $key . ' is empty \n';
                }
            }
        }
        return $res;
    }

    public function getFromFormation($request)
    {

    }
}

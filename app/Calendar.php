<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    public $timestamps= false;
    protected $fillable = ['formation', 'date', 'duree', 'heure_debut', 'intitule', 'lieu', 'enseignant'];

}
